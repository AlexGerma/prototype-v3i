//
//  SecondViewController.swift
//  V3iPrototype
//
//  Created by Alexis German Hurtado on 10/20/20.
//

import UIKit

class  DeviceConnectionViewController: UIViewController {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var buttonTryAgain: UIButton!
    @IBOutlet weak var buttonContinue: UIButton!
    
    var service = httpService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "STEP 2"
        self.service = httpService()
        self.startServices()
    }
    
    func startServices() {
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        DispatchQueue.main.async {
            self.onService()
        }
    }
   
    func onService() {
        self.service.getVersion() { stateResponse in
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            if stateResponse {
                self.buttonTryAgain.isHidden = true
                self.labelMessage.text = "Your controller successfully connected. you can now continue with the configuration."
                self.labelMessage.textColor = .black
                self.labelTitle.text = "SUCCEEDED CONNECTION"
                self.buttonContinue.isHidden = false
                return
            }
            self.buttonTryAgain.isHidden = false
            self.labelMessage.text = "There was a problem connecting your controller. Verify that the Wi-Fi network is correct and try again please."
            self.labelMessage.textColor = .red
            self.buttonContinue.isHidden = true
            self.labelTitle.text = "ERROR CONNECTION"
        }
    }
    
    @IBAction func onTryAgain(_ sender: Any) {
        self.buttonTryAgain.isHidden = true
        self.labelMessage.text = "Wait a moment, we are verifying the connection of the v3i to the device."
        self.labelMessage.textColor = .black
        self.labelTitle.text = "VERIFYING THE CONNECTION"
        self.buttonContinue.isHidden = true
        self.startServices()
    }
        
  
    
//    @IBAction func onBackButton(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }
}
