//
//  ManageHTTPservice.swift
//  V3iPrototype
//
//  Created by Alexis German Hurtado on 11/5/20.
//

import Alamofire
import Foundation

class httpService {
    
    var wifiList: [V3iNetwork] = []
    var alamofire: Alamofire.Session?
    
    init() {
        alamofire = AF
    }
    
    func getVersion(callback: @escaping (_ state: Bool) -> Void) {
        alamofire!.request(BASE_URL+VERSION_PRODUCT, method: .get).response { resp in
            print("--------------")
            debugPrint(resp)
            debugPrint(resp.response as Any)
            
            guard let newResponse = resp.response else {
                callback(false)
                return
            }
            
            if ((newResponse.statusCode as Int) == 200) {
                callback(true)
            } else {
               callback(false)
            }
        }
    }
    
    func getListWifi(callback: @escaping (_ state: Bool,_ list: [V3iNetwork]) -> Void) {
        alamofire!.request(BASE_URL+LIST_WIFI, method: .get).response { resp in
            print("--------------")
            debugPrint(resp)
            debugPrint(resp.response as Any)
          
            guard let newResponse = resp.response else {
                callback(false, [])
                return
            }

            if ((newResponse.statusCode as Int) == 200) {
                guard let data = resp.data else { return }
                let dataString: String = String(data: data, encoding: .utf8)!
                let array = dataString.components(separatedBy: ";")
                for item in array {
                    if item == "X" {
                        break
                    }
                    let SSID = item.substring(fromIndex: 1)
                    let securityType = V3iSecurityType.parseString(type: item.substring(toIndex: 1))
                    self.wifiList.append(V3iNetwork(ssid: SSID, securityType: securityType))
                }
                callback(true, self.wifiList)
            } else {
               callback(false, [])
            }
        }
    }
}

struct V3iNetwork {
    let ssid: String
    let securityType: V3iSecurityType
}

enum V3iSecurityType {
   case OPEN
   case WPA1
   case WEP
   case WPA2
   case UNKNOWN

   static func parseString(type: String) -> V3iSecurityType {
        switch (type) {
        case "0" : return OPEN
        case "1" : return WEP
        case "2" : return WPA1
        case "3" : return WPA2
        default:
            return UNKNOWN
        }
    }
    
}


