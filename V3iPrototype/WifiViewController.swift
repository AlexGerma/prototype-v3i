//
//  ThreeViewController.swift
//  V3iPrototype
//
//  Created by Alexis German Hurtado on 10/20/20.
//

import UIKit

class WifiViewController: UIViewController {
    let simple = ["hola hoooo","hell odajwio o", "numero 3", "numero 4" , "numero 5", "numero 6", "numero 7", "numero 8","numero 9","numero 10","numero 11s"]
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "STEP 3"
    }
    
    let service = httpService()
    
    @IBAction func onChooseNetwork(_ sender: Any) {
        dialogListWifi()
    }
    
    func dialogListWifi() {
        self.service.getListWifi(callback: { state, list in
            if state {
                self.makeDialogList(list: list)
            }
        })
    }
    
    func makeDialogList(list: [V3iNetwork]) {
       let alert = UIAlertController(title: "Select Network", message: "", preferredStyle: .alert)
       list.forEach({ data in
        alert.addAction(UIAlertAction(title: data.ssid, style: .default, handler: { action in
            print(data)
            }))
        })
        self.present(alert, animated: true, completion: nil)
    }
    
}
