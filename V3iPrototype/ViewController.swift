//
//  ViewController.swift
//  V3iPrototype
//
//  Created by Alexis German Hurtado on 10/20/20.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "STEP 1"
        // Do any additional setup after loading the view.
    }
      
    @IBAction func readUserData(_ sender: Any) {
//       self.logLabel.text = "listening for services..."
//       print("listening for services...")
//       self.services.removeAll()
//       self.nsb = NetServiceBrowser()
//       self.nsb.delegate = self
//       self.nsb.searchForServices(ofType:"_http._tcp", inDomain: "")
    }
    
    @IBAction func onOpenWifi(_ sender: Any) {
        if let url = URL(string:UIApplication.openSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }

    @IBAction func onContinue(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : DeviceConnectionViewController = storyboard.instantiateViewController(withIdentifier: "deviceConnection") as! DeviceConnectionViewController
        self.show(viewController, sender: self)
    }
    
}

